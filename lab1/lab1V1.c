#include "matrix.c"
#include <time.h>

#include "stdio.h"
//#include "mpi.h"
#include "stdlib.h"
#include "math.h"
int ProcNum;
int ProcRank;

#define _MAX_VALUE 10
#define _N_DEFAULT 4
#define _R 0.001
#define _E 0.0001


int main(char argc, char* argv[]) {
    time_t timeStart = time(NULL);
    /////////////////////////////////////////////////////////////////////////
    //создание задачи, которая имеет решение
    Matrix *A, *B, *answerX;
    createSolution(&A, &B, &answerX, _N_DEFAULT, _MAX_VALUE);
    ///////////////////////////////////////////////////////////////////////// 

    /////////////////////////////////////////////////////////////////////////
    //приступаем к решению задачи
    Matrix *x = createMatrix(1, _N_DEFAULT);
    randomMatrix(x, _MAX_VALUE);
    double r = _R;
    double e = _E;
    double matrixAccuracy = 1000000, matrixAccuracyNext;
    Matrix *AxX, *AxXsubB, *mulR, *nextX;
    
    //решаем задачу
    do {
        AxX = multiplication(A, x);
        AxXsubB = subtraction(AxX, B);
        mulR = multiplicationConst(AxXsubB, r);
        nextX = subtraction(x, mulR);
        matrixAccuracyNext = matrixNorm(AxXsubB) / matrixNorm(B);
        if (matrixAccuracy < matrixAccuracyNext) {
			matrixAccuracy = matrixAccuracyNext;
            deleteMatrix(nextX);
            r = -r;
        }
        else {
            deleteMatrix(x);
            x = nextX;
			matrixAccuracy = matrixAccuracyNext;
        }
        deleteMatrix(AxX);
        deleteMatrix(AxXsubB);
        deleteMatrix(mulR);
    } while (matrixAccuracyNext > e);

    printMatrix(x, "ANSWER");
    deleteMatrix(x);
    deleteMatrix(A);
    deleteMatrix(B);
    deleteMatrix(answerX);

    time_t timeEnd = time(NULL);
    printf("program take %d sec", timeEnd - timeStart);

    //scanf("%c");
    return 0;
}