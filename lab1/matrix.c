#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "matrix.h"


/////////////////////////////////////////////////////////////////////////
Matrix* createMatrix(int sizeX, int sizeY){
    Matrix *newMatrix = (Matrix *)malloc(sizeof(Matrix));
    newMatrix->sizeX = sizeX;
    newMatrix->sizeY = sizeY;
    newMatrix->matrix = (double **)malloc(sizeof(double *) * sizeX);
    double *array = (double *)malloc(sizeof(double) * sizeX * sizeY);
    for (int i = 0; i < sizeX; i++)
        newMatrix->matrix[i] = &array[i * sizeY];
    return newMatrix;
}
void deleteMatrix(Matrix *matrix){
    free(matrix->matrix[0]);
    free(matrix->matrix);
    free(matrix);
    return;
}
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
void printMatrix(Matrix *matrix, const char name[]){
    printf("matrix name is %s\n", name);
    for (int i = 0; i < matrix->sizeX; i++){
        for (int j = 0; j < matrix->sizeY; j++)
            printf("%lf ", matrix->matrix[i][j]);
        printf("\n");
    }
    return;
}
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
void setZero(Matrix *matrix){
    for (int i = 0; i < matrix->sizeX; i++)
        for (int j = 0; j < matrix->sizeY; j++)
            matrix->matrix[i][j] = 0;
    return;
}
void randomMatrix(Matrix* matrix, int maxValue, time_t time){
    double value;
    for (int i = 0; i < matrix->sizeX; i++)
        for (int j = 0; j < matrix->sizeY; j++)
            matrix->matrix[i][j] = rand(time) % maxValue;
    return;
}
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
double scalarMultiplication(Matrix *A, Matrix *B){
    double result = 0;
    for (int i = 0; i < A->sizeY; i++)
        result += A->matrix[0][i] * B->matrix[0][i];
    return result;
}
Matrix *multiplication(Matrix *A, Matrix *B){
    Matrix *AxB = createMatrix(B->sizeX, A->sizeY);
    setZero(AxB);
    for (int i = 0; i < B->sizeX; i++)
        for (int j = 0; j < A->sizeY; j++)
            for (int k = 0; k < B->sizeY; k++)
                AxB->matrix[i][j] = AxB->matrix[i][j] + A->matrix[k][j] * B->matrix[i][k];
    return AxB;
}
Matrix *subtraction(Matrix *A, Matrix *B){
    Matrix *AsubB = createMatrix(A->sizeX, A->sizeY);
    setZero(AsubB);
    for (int i = 0; i < A->sizeX; i++)
        for (int j = 0; j < A->sizeY; j++)
            AsubB->matrix[i][j] = A->matrix[i][j] - B->matrix[i][j];
    return AsubB;
}
Matrix *multiplicationConst(Matrix *A, double Const){
    Matrix *AxConst = createMatrix(A->sizeX, A->sizeY);
    setZero(AxConst);
    for (int i = 0; i < A->sizeX; i++)
        for (int j = 0; j < A->sizeY; j++)
            AxConst->matrix[i][j] = A->matrix[i][j] * Const;
    return AxConst;
}
double matrixNorm(Matrix *A){
    double result = 0;
    for (int i = 0; i < A->sizeX; i++)
        for (int j = 0; j < A->sizeY; j++)
            result += A->matrix[i][j] * A->matrix[i][j];
    return sqrt(result);
}
/////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////
void createSolution(Matrix **A, Matrix **B, Matrix **answerX, 
                    int _N_DEFAULT, int _MAX_VALUE, time_t time) {
    *A = createMatrix(_N_DEFAULT, _N_DEFAULT);
    *answerX = createMatrix(1, _N_DEFAULT);
    randomMatrix(*A, _MAX_VALUE, time);
    randomMatrix(*answerX, _MAX_VALUE, time);
    *B = multiplication(*A, *answerX);
    printMatrix(*A, "A");
    printMatrix(*answerX, "x");
    printMatrix(*B, "B");
    //return;
}
/////////////////////////////////////////////////////////////////////////

