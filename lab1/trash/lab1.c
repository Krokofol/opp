#include "matrix.c"
#include <time.h>

#include "stdio.h"
#include "mpi.h"
#include "stdlib.h"
#include "math.h"
int ProcNum;
int ProcRank;

#define _MAX_VALUE 2
#define _N_DEFAULT 8
#define _R 0.01
#define _E 0.00001

void sendDoubleArray(double* array, int n, int ProcNumber){
    MPI_Send(array, _N_DEFAULT, MPI_DOUBLE, 0, ProcNumber * 10 + 1, MPI_COMM_WORLD);
    return;
}
void getDoubleArray(double* array, int n, int ProcNumber){
    MPI_Recv(array, _N_DEFAULT, MPI_DOUBLE, 0, ProcNumber * 10 + 2, MPI_COMM_WORLD);
    return;
}
void sendMatrix(Matrix* matrix, int ProcNumber, int ProcCount){
    

    return;
}


int main(char argc, char* argv[]) {
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
    MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
    Matrix *A, *B, *answerX;
    Matrix *AxX, *AxXsubB, *mulR, *nextX;
    Matrix *x;

    if (ProcNum == 0){
        time_t timeStart = time(NULL);
        createSolution(&A, &B, &answerX, _N_DEFAULT, _MAX_VALUE);
        x = createMatrix(1, _N_DEFAULT);
        randomMatrix(x, _MAX_VALUE);
        double r = _R;
        double e = _E;
        double matrixAccuracy = 1000000, matrixAccuracyNext;
    }
    else {
        A = createMatrix(_N_DEFAULT, _N_DEFAULT);
        free(A->matrix[0]);
        B = createMatrix(1, _N_DEFAULT);
        free(B->matrix[0]);
        x = createMatrix(1, _N_DEFAULT);
        free(x->matrix[0]);
    }

    //решаем задачу
    do {
        if (ProcNum == 0){
            
        }
        else {

        }
        AxX = multiplication(A, x);
        AxXsubB = subtraction(AxX, B);
        mulR = multiplicationConst(AxXsubB, r);
        nextX = subtraction(x, mulR);
        matrixAccuracyNext = matrixNorm(AxXsubB) / matrixNorm(B);
        if (matrixAccuracy < matrixAccuracyNext) {
			matrixAccuracy = matrixAccuracyNext;
            deleteMatrix(nextX);
            r = -r;
        }
        else {
            deleteMatrix(x);
            x = nextX;
			matrixAccuracy = matrixAccuracyNext;
        }
        deleteMatrix(AxX);
        deleteMatrix(AxXsubB);
        deleteMatrix(mulR);
    } while (matrixAccuracyNext > e);

    printMatrix(x, "ANSWER");
    deleteMatrix(x);
    deleteMatrix(A);
    deleteMatrix(B);
    deleteMatrix(answerX);

    time_t timeEnd = time(NULL);
    printf("program take %d sec", timeEnd - timeStart);

    //scanf("%c");
    return 0;
}