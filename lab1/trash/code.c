//#include <matrix.h>
#define _MAX_VALUE 10
#define _N_DEFAULT 2

//#include <matrix.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

//#pragma once

typedef struct {
    double **matrix;
    int sizeX;
    int sizeY;
} Matrix;

//memory
Matrix *createMatrix(int sizeX, int sizeY);
void deleteMatrix(Matrix *matrix);

//usefull
//вывести матрицу
void printMatrix(Matrix *matrix, const char name[]);

//initialization
//инициализация матрицы нулями
void setZero(Matrix *matrix);
//инициализация случайными числами (не больше maxValue)
void randomMatrix(Matrix *matrix, int maxValue);

//operations
//произведение матриц А и B (A*B)
Matrix *multiplication(Matrix *A, Matrix *B);
//разность матриц А и В (А-В)
Matrix *subtraction(Matrix *A, Matrix *B);
//произведение матрицы А и const (const*A)
Matrix *multiplicationConst(Matrix *A, int Const);
//получаем норму матрицы
double matrixNorm(Matrix *A);


/////////////////////////////////////////////////////////////////////////
Matrix* createMatrix(int sizeX, int sizeY){
    Matrix *newMatrix = (Matrix*)malloc(sizeof(Matrix));
    newMatrix->sizeX = sizeX;
    newMatrix->sizeY = sizeY;
    newMatrix->matrix = (double **)malloc(sizeof(double *) * sizeX);
    double *array = (double *)malloc(sizeof(double) * sizeX * sizeY);
    for (int i = 0; i < sizeX; i++)
        newMatrix->matrix[i] = &array[i * sizeY];
    return newMatrix;
}
void deleteMatrix(Matrix *matrix){
    free(matrix->matrix[0]);
    free(matrix->matrix);
    free(matrix);
    return;
}
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
void printMatrix(Matrix *matrix, const char name[]){
    printf("matrix name is %s\n", name);
    for (int i = 0; i < matrix->sizeX; i++){
        for (int j = 0; j < matrix->sizeY; j++)
            printf("%d ", matrix->matrix[i][j]);
        printf("\n");
    }
    return;
}
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
void setZero(Matrix *matrix){
    for (int i = 0; i < matrix->sizeX; i++)
        for (int j = 0; j < matrix->sizeY; j++)
            matrix->matrix[i][j] = 0;
    return;
}
void randomMatrix(Matrix* matrix, int maxValue){
    for (int i = 0; i < matrix->sizeX; i++)
        for (int j = 0; j < matrix->sizeY; j++)
            matrix->matrix[i][j] = rand() % maxValue;
    return;
}
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
Matrix *multiplication(Matrix *A, Matrix *B){
    Matrix *AxB = createMatrix(B->sizeX, A->sizeY);
    setZero(AxB);
    for (int i = 0; i < B->sizeX; i++)
        for (int j = 0; j < A->sizeY; j++)
            for (int k = 0; k < B->sizeY; k++)
                AxB->matrix[i][j] += A->matrix[k][j] * B->matrix[i][k];
    return AxB;
}
Matrix *subtraction(Matrix *A, Matrix *B){
    Matrix *AsubB = createMatrix(A->sizeX, A->sizeY);
    setZero(AsubB);
    for (int i = 0; i < A->sizeX; i++)
        for (int j = 0; j < A->sizeY; j++)
            AsubB->matrix[i][j] = A->matrix[i][j] - B->matrix[i][j];
    return AsubB;
}
Matrix *multiplicationConst(Matrix *A, int Const){
    Matrix *AxConst = createMatrix(A->sizeX, A->sizeY);
    setZero(AxConst);
    for (int i = 0; i < A->sizeX; i++)
        for (int j = 0; j < A->sizeY; j++)
            AxConst->matrix[i][j] = A->matrix[i][j] * Const;
    return AxConst;
}
double matrixNorm(Matrix *A){
    double result = 0;
    for (int i = 0; i < A->sizeX; i++)
        for (int j = 0; j < A->sizeY; j++)
            result += A->matrix[i][j] * A->matrix[i][j];
    return sqrt(result);
}
/////////////////////////////////////////////////////////////////////////


int main(char argc, char* argv[]){

    //создание задачи, которая имеет решение
    int n = _N_DEFAULT;
    Matrix *A = createMatrix(n, n);
    Matrix *answerX = createMatrix(1, n);
    randomMatrix(A, _MAX_VALUE);
    randomMatrix(answerX, _MAX_VALUE);
    Matrix *B = multiplication(A, answerX);
    printMatrix(A, "A");
    printMatrix(answerX, "x");
    printMatrix(B, "B");

    //приступаем к решению задачи
    Matrix *x = createMatrix(1, n);
    randomMatrix(x, _MAX_VALUE);
    double r = 0.01;
    double e = 0.000001;
    double matrixAccuracy = 1000000, matrixAccuracyNext;

    //решаем задачу
    Matrix *AxX, *AxXsubB, *mulR, *nextX;
    do {
        AxX = multiplication(A, x);
        AxXsubB = subtraction(AxX, B);
        mulR = multiplicationConst(AxXsubB, r);
        nextX = subtraction(x, mulR);
        
        matrixAccuracyNext = matrixNorm(AxXsubB) / matrixNorm(B);
        
        if (matrixAccuracy < matrixAccuracyNext) {
            deleteMatrix(nextX);
            r = -r;
        }
        else {
            deleteMatrix(x);
            x = nextX;
        }
        
        deleteMatrix(AxX);
        deleteMatrix(AxXsubB);
        deleteMatrix(mulR);
    } while (matrixAccuracyNext > e);

    printMatrix(x, "ANSWER");
    
    deleteMatrix(x);
    deleteMatrix(A);
    deleteMatrix(B);
    deleteMatrix(answerX);
    return 0;
}