#pragma once

/////////////////////////////////////////////////////////////////////////
typedef struct{
    double **matrix;
    int sizeX;
    int sizeY;
} Matrix;
//memory
Matrix *createMatrix(int sizeX, int sizeY);
void deleteMatrix(Matrix *matrix);
//usefull
//вывести матрицу
void printMatrix(Matrix *matrix, const char name[]);
//initialization
//инициализация матрицы нулями
void setZero(Matrix *matrix);
//инициализация случайными числами (не больше maxValue)
void randomMatrix(Matrix *matrix, int maxValue);
//operations
//сколярное произведение матриц
double scalarMultiplication(Matrix *A, Matrix *B);
//произведение матриц А и B (A*B)
Matrix *multiplication(Matrix *A, Matrix *B);
//разность матриц А и В (А-В)
Matrix *subtraction(Matrix *A, Matrix *B);
//произведение матрицы А и const (const*A)
Matrix *multiplicationConst(Matrix *A, double Const);
//получаем норму матрицы
double matrixNorm(Matrix *A);
//создание решения
void createSolution(Matrix **A, Matrix **B, Matrix **answerX, int _N_DEFAULT, int _MAX_VALUE);
/////////////////////////////////////////////////////////////////////////