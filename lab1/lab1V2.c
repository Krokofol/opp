#include "matrix.c"

#define _MAX_VALUE 10
#define _N_DEFAULT 8
#define _E 0.2392
#define _TIME 0
//#define _TIME timeStart
//#define _R 0.01

int main(){

    time_t timeStart = time(NULL);
    Matrix *A, *B, *answerX;
    createSolution(&A, &B, &answerX, _N_DEFAULT, _MAX_VALUE, _TIME);

    Matrix *AxX, *Y, *X, *nextX, *AxY, *RxY, *AXXsubB;
    double R, accuracy;

    X = createMatrix(1, _N_DEFAULT);
    randomMatrix(X, _MAX_VALUE, 0);
    int counter = 0;
    do {
		//AxX = A * X;
		AxX = multiplication(A, X);
        //Y = AxX - B = A * X - B;
        Y = subtraction(AxX, B);
        //AxY = A * Y;
        AxY = multiplication(A, Y);
        //R = (Y, AxY) / (AxY, AxY)
        R = scalarMultiplication(Y, AxY) / scalarMultiplication(AxY, AxY);
        //RxY = R * Y;
        RxY = multiplicationConst(Y, R);
        //X = X - RxY = X - R * Y;
        nextX = subtraction(X, RxY);
        deleteMatrix(X);
        X = nextX;
        //AxX = A * X;
		deleteMatrix(AxX);
        AxX = multiplication(A, X);
        //AXXsubB = AxX - B = A * X - B;
        AXXsubB = subtraction(AxX, B);
        //accuracy = ||AxX - B|| / ||B|| = ||A * X - B|| / ||B||;
        accuracy = matrixNorm(AXXsubB) / matrixNorm(B);

        if (counter % 1000 == 0)
            printf("%lf\n", accuracy);
        counter++;

        deleteMatrix(AXXsubB);
        deleteMatrix(Y);
        deleteMatrix(AxY);
        deleteMatrix(RxY);
        deleteMatrix(AxX);
    } while (accuracy > _E);

    printMatrix(X, "I_GOT_X");

    deleteMatrix(A);
    deleteMatrix(B);
    deleteMatrix(answerX);
    deleteMatrix(X);
    return 0;
}