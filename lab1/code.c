#include <math.h>
#include <stdio.h>
//#include <mpi.h>

#define N 5
double *mul_A_x(double *A, double *x, int size)
{
    double *Ax = (double *)calloc(size, sizeof(double));
    for (int i = 0; i < size; i++)
        for (int j = 0; j < N; j++)
            Ax[i] += A[i * N + j] * x[j];
    return Ax;
}
double count_b_mod(double *b)
{
    double mod = 0;
    for (int i = 0; i < N; i++)
        mod += b[i] * b[i];
    return sqrt(mod);
}
double count_Ax_mod(double *A, double *x, double *b, int size)
{
    double mod = 0;
    double *Ax = mul_A_x(A, x, size);
    for (int i = 0; i < size; i++)
    {
        mod += (Ax[i] - b[i]) * (Ax[i] - b[i]);
    }
    free(Ax);
    return sqrt(mod);
}

void check_value(double *x, double e)
{
    for (int i = 0; i < N; i++)
        if (x[i] - 1.0 >= e)
        {
            printf("wrong answer");
            return;
        }
}

void normal()
{
    double t = 0.01;
    double e = 0.00001;
    double *A = (double *)calloc(N * N, sizeof(double));
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            if (i == j)
                A[i * N + j] = 2.0;
            else
                A[i * N + j] = 1.0;
    double *b = (double *)calloc(N, sizeof(double));
    for (int j = 0; j < N; j++)
        b[j] = N + 1;
    double mod_b = count_b_mod(b);

    double *x = (double *)calloc(N, sizeof(double));
    double arg = count_Ax_mod(A, x, b, N) / mod_b;
    char change = 0;
    int iter = 0;
    while (arg >= e)
    {
        iter++;
        double *mul;
        mul = mul_A_x(A, x, N);
        for (int i = 0; i < N; i++)
            x[i] -= t * (mul[i] - b[i]);
        free(mul);
        double temp = count_Ax_mod(A, x, b, N) / mod_b;
        if (arg < temp)
        {
            if (!change)
            {
                change++;
                t *= -1;
            }
            else
                printf("не сходится");
            break;
        }
        arg = temp;
    }
    check_value(x, e);
    for (int i = 0; i < N; i++)
        printf("%lf ", x[i]);
}