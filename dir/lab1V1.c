#include "matrix.c"
#include <time.h>
#include <mpi.h>
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
int ProcRank;
int ProcSize;

#define _MAX_VALUE 10
#define _N_DEFAULT 1200
#define _R 0.001
#define _E 0.0001


int main(char argc, char* argv[]) {
    MPI_Init(NULL, NULL);

    MPI_Comm_size(MPI_COMM_WORLD, &ProcSize);
    MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);

    time_t timeStart = time(NULL);
    /////////////////////////////////////////////////////////////////////////
    //создание задачи, которая имеет решение
    Matrix *A, *B, *answerX;
    createSolution(&A, &B, &answerX, _N_DEFAULT, _MAX_VALUE);
    /////////////////////////////////////////////////////////////////////////
        
        
    /////////////////////////////////////////////////////////////////////////
    //приступаем к решению задачи
    Matrix *x = createMatrix(1, _N_DEFAULT);
    randomMatrix(x, _MAX_VALUE);
    //printMatrix(x, "RANDOMED_X_(START)");
    double r = _R;
    double e = _E;
    double matrixAccuracy = 1000000, matrixAccuracyNext;
    Matrix *AxX, *AxXsubB, *mulR, *nextX;
    int counter = 0;
    //решаем задачу
    char end = 1;
    do {
        counter++;
        AxX = multiplication(A, x);
        AxXsubB = subtraction(AxX, B);
        mulR = multiplicationConst(AxXsubB, r);
        nextX = subtraction(x, mulR);
        matrixAccuracyNext = matrixNorm(AxXsubB) / matrixNorm(B);
        if (matrixAccuracy < matrixAccuracyNext) {
			matrixAccuracy = matrixAccuracyNext;
            deleteMatrix(nextX);
            r = -r;
        }
        else {
            deleteMatrix(x);
            x = nextX;
			matrixAccuracy = matrixAccuracyNext;
        }
        deleteMatrix(AxX);
        deleteMatrix(AxXsubB);
        deleteMatrix(mulR);
        for (int i = 0; i < x->sizeX; i++)
            for (int j = 0; j < x->sizeY; j++)
                if (!(x->matrix[i][j] == x->matrix[i][j])) end = 0;
    } while ((end)&&(matrixAccuracyNext > e));

    printMatrix(x, "ANSWER");
    deleteMatrix(x);
    deleteMatrix(A);
    deleteMatrix(B);
    deleteMatrix(answerX);

    time_t timeEnd = time(NULL);
    printf("program take %d sec, ", timeEnd - timeStart);
    printf("%d iterations\n", counter);

    //scanf("%c");
    return 0;
}